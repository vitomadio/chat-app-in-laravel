<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Contact Routes
Route::get('/contacts', 'ContactsController@get');

// Messages Routes
Route::get('/conversation/{id}', 'MessagesController@getMessagesFor');
Route::post('/conversation/writing', 'MessagesController@writing');
Route::post('/conversation/send', 'MessagesController@send');
Route::post('/conversation/delete', 'MessagesController@deleteConversation');
Route::post('/conversation/send-file', 'MessagesController@saveFile');
Route::post('/conversation/download-file', 'MessagesController@downloadFile');