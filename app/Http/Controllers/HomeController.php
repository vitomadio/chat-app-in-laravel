<?php

namespace App\Http\Controllers;

use App\LastContact;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $sessionUser = auth()->user();
        // dd($sessionUser);
        $lastContact = LastContact::where('user_id', auth()->id())->with('contact')->first();
        // dd($lastContact->contact);

        return view('home', ['lastContact' => $lastContact->contact]);
    }
}
