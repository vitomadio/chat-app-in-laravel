<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Events\WritingMessage;
use Illuminate\Http\Request;
use App\Message;
use App\Services\ContactService;
use App\Upload;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MessagesController extends ContactsController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->middleware('auth');
        $this->contactService = $contactService;
    }

    // Get chat messages.
    public function getMessagesFor($id)
    {
        // Select contact. Saves the contact id to open next time reconnect.
        $this->contactService->selectContact($id);
        
        // Update messages of the selected contact as readed.
        Message::where('from', $id)->where('to', auth()->id())->update(['read' => true]);

        $messages = Message::with('uploads')->where(function ($q) use ($id) {
            $q->where('deleted', '!=', auth()->id())->orWhereNull('deleted');
            $q->where('from', auth()->id());
            $q->where('to', $id);
        })->orWhere(function ($q) use ($id) {
            $q->where('deleted', '!=', auth()->id())->orWhereNull('deleted');
            $q->where('to', auth()->id());
            $q->where('from', $id);
        })->get(); //(a = 1 And b = 2 ) OR (c = 1 OR d = 2)
        
        return response()->json($messages);
    }

    // Broadcast event while writing.
    public function writing(Request $request)
    {
        $contactId = $request->contact_id;
        $isTyping = $request->isTyping;
        event(new WritingMessage($contactId, $isTyping));
    }

    // Create and send message.
    public function send(Request $request)
    {
        $message = Message::create([
            'from' => auth()->id(),
            'to' => $request->contact_id,
            'text' => $request->text,
            'deleted' => null
        ]);

        broadcast(new NewMessage($message));

        return response()->json($message);
    }

    // Delete Chat conversation.
    public function deleteConversation(Request $req)
    {
        $contactId = $req->contactId;

        $messages = Message::where(function ($query) use ($contactId) {
            $query->where('deleted', '!=', auth()->id())->orWhereNull('deleted');
            $query->where('to', $contactId);
            $query->where('from', auth()->id());
        })->orWhere(function ($query) use ($contactId) {
            $query->where('deleted', '!=', auth()->id())->orWhereNull('deleted');
            $query->where('to', auth()->id());
            $query->where('from', $contactId);
        })->get();

        foreach ($messages as $message) {
            if ($message->deleted == null) {
                $message->update(['deleted' => auth()->id()]);
            } else {
                foreach($message->uploads()->get() as $upload){
                    $file = explode('/', $upload->file_url,3);
                    Storage::disk('uploads')->delete($file);
                    $upload->delete();
                }
                $message->delete();
            }
        }
    }
    // Save Files to storage.
    public function saveFile(Request $request)
    {
        $userId = auth()->id();
        $files = $request->file('files');

        $message = new Message;
        $upload = new Upload;

        $message->from = $userId;
        $message->to = $request->contact_id;
        $message->text = $request->text;
        

        DB::transaction(function () use ($message, $upload, $files) {
            $message->save();

            // Store each file.
            foreach ($files as $file) {
                $fileExtension = $file->getClientOriginalExtension();
                $fileName = $file->getClientOriginalName();
                $filePath = Storage::disk('uploads')->put(null, $file);

                $upload = Upload::create([
                    'message_id' => $message->id,
                    'file_url' => '/uploads/' . $filePath,
                    'file_name' => $fileName,
                    'file_extension' => $fileExtension
                ]);
                $message->uploads()->save($upload);

            }
        });
        $message->load('uploads');
        broadcast(new NewMessage($message));
        return response()->json($message);
    }
    // Download a file.
    public function downloadFile(Request $request){
        $fileUrl = $request->file['file_url'];
        
        $extension = explode('.',$fileUrl,2)[1]; // Extension of the file to be used in headers.
        $headers = ['Content-Type' => 'application/'.$extension];

        // Check for the file existence.
        $exists = Storage::disk('uploads')->exists(explode('/',$fileUrl,3)[2]);
        if($exists){
            return response($fileUrl, 200 , $headers);
        }
    }
}
