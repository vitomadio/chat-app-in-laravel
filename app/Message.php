<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Upload;

class Message extends Model
{
    protected $guarded = [];
    // Relation to get the user who sent the message.
    public function fromContact()
    {
        return $this->hasOne(User::class, 'id', 'from');
    }
    // Relation to set the uploads.
    public function uploads()
    {
        return $this->hasMany(Upload::class);
    }
}
