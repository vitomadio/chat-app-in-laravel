<?php

namespace App\Services;

use App\LastContact;

class ContactService
{

  public function selectContact($id)
  {
    // Find if last contact exists.
    $lastContact = LastContact::where('user_id', auth()->id());

    if ($lastContact->exists()) {
      $lastContact->update(['contact_id' => $id]);
    } else {
      LastContact::create([
        'user_id' => auth()->id(),
        'contact_id' => $id
      ]);
    }

  }
}
