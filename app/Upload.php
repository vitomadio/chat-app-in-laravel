<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Message;

class Upload extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function message(){
        return $this->belongsTo(Message::class);
    }
}
