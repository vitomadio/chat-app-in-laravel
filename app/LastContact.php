<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class LastContact extends Model
{
    protected $guarded = [];

    // protected $with = ['contact'];

    public function contact() {
        return $this->hasOne(User::class, 'id', 'contact_id');
    }
}
