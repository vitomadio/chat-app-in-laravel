const messageb = document.querySelector('.message-b');
const messaged = document.querySelector('.message-d');
  
const setTimeOne = setTimeout(() => {
  messageb.style.display = 'none';
}, 4800);
const setTimeTwo = setTimeout(() => {
  messaged.style.display = 'block'
}, 5500);
const setTimeThree = setTimeout(() => {
  messaged.style.display = 'none';
}, 9400);

// Set interval to infinite loop.
setInterval(() => {
  //Cleat previous setTimeouts.
  clearTimeout(setTimeOne);
  clearTimeout(setTimeTwo);
  clearTimeout(setTimeThree);
  //reset messages.
  messageb.style.display = 'block';
  messaged.style.display = 'block';
  setTimeout(() => {
    messageb.style.display = 'none';
  }, 4800);
  setTimeout(() => {
    messaged.style.display = 'block'
  }, 5500);
  setTimeout(() => {
    messaged.style.display = 'none';
  }, 9400);
}, 12000);




