# Chat App.
This is a simple chat app made in **Laravel** using Pusher service, and **Vue.js** as front. With Chat App you can send and receive private messages, and also share images or files with other users.
Feel free to clone this repo and start playing with it.

```
$ cd <project folder>
$ composer install
$ npm install
```
* Change **.env.example** file name to **.env** and replace the following fields with your database configuration.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<database_name>
DB_USERNAME=<username>
DB_PASSWORD=<password>
```
* Generate your **APP_KEY** automatically as follow:
```
$ php artisan key:generate
```
* Signup to Pusher.com and get the credentials for this fields.
```
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1
```
* Generate authentication system.
```
$ php artisan make:auth
```
* Run migration.
```
$ php artisan migrate
```
* Generate fake users and messages.
```
$ php artisan tinker
>> factory(App\User::class, 15)->create();
...
>> factory(App\Message::class, 150)->create();
```
#### Almost there...
```
$ php artisan serve
```
Now go to http://localhost:8000 and have some fun :smirk:.



