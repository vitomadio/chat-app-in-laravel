<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Chat App</title>

    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600"
        rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style lang="scss">
        @import '../assets/sass/variables';

        html,
        body {
            background-color: #4A5080;
            font-family: 'Raleway', sans-serif;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: space-evenly;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
            color: #C7D2FF;
            font-weight: 100;
        }

        .links>a {
            color: #fff;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .container>p {
            color: #4d4d4d !important;
            margin: 0 !important;
        }

        small {
            color: #4d4d4d;
            float: right;
        }

        .container {
            max-width: 460px !important;
            background: #C7D2FF;
            border-radius: 12px;
            animation: boing 1s forwards;
            box-shadow: 0px 0px 8px rgba(0, 0, 0, .4);
        }

        @keyframes boing {
            0% {
                transform: scaleY(0);
            }

            40% {
                transform: scaleY(1);
            }

            60% {
                transform: scaleY(.6);
            }

            75% {
                transform: scaleY(1);
            }

            90% {
                transform: scaleY(.8);
            }

            100% {
                transform: scaleY(1);
            }
        }

        .wrapper {
            overflow: hidden;
        }

        .from {
            background: #FFFCAC;
            border-radius: 0 12px 12px 12px;
            padding: 1em;
            box-shadow: 3px 3px 5px rgba(0, 0, 0, .3);
            position: relative;
        }

        .to {
            background: #99ADFF;
            border-radius: 12px 0 12px 12px;
            padding: 1em;
            box-shadow: -3px 3px 5px rgba(0, 0, 0, .3);
            position: relative;
        }

        .input {
            background: #fff;
            border-radius: 12px;
            height: 50px;
        }

        .triangle {
            width: 0px;
            height: 0px;
            border-style: solid;
            border-width: 16px;
            border-left-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            position: absolute;
        }

        .from>.triangle {
            border-top-color: #FFFCAC;
            top: 0;
            left: -13px;
        }

        .to>.triangle {
            border-top-color: #99ADFF;
            top: 0;
            right: -13px;
        }

        .conversation-wrapper {
            transform: translateY(100%);
            animation: chat 12s steps(5) infinite forwards;
        }

        @keyframes chat {
            to {
                transform: translateY(-25%);
            }
        }

        .message-b {
            opacity: 0;
            float: left;
            overflow: hidden;
            border-right: .15em solid orange;
            /* The typwriter cursor */
            white-space: nowrap;
            /* Keeps the content on a single line */
            margin: 0 auto;
            /* Gives that scrolling effect as the typing happens */
            letter-spacing: .05em;
            /* Adjust as needed */
            animation:
                typing 1.2s steps(13, end) 3.5s infinite,
                blink-caret .75s step-end infinite;
        }

        /* The typing effect */
        @keyframes typing {
            0% {
                width: 0%;
                opacity: 1;
            }

            100% {
                width: 28%;
                opacity: 1;
            }

        }

        /* The typewriter cursor effect */
        @keyframes blink-caret {

            from,
            to {
                border-color: transparent
            }

            50% {
                border-color: orange;
            }
        }

        .message-d {
            opacity: 0;
            float: left;
            overflow: hidden;
            /* Ensures the content is not revealed until the animation */
            border-right: .15em solid orange;
            /* The typwriter cursor */
            white-space: nowrap;
            /* Keeps the content on a single line */
            margin: 0 auto;
            /* Gives that scrolling effect as the typing happens */
            letter-spacing: .05em;
            /* Adjust as needed */
            animation:
                typing-b 1.2s steps(16, end) 8.2s infinite,
                blink-caret .75s step-end infinite;
        }

        /* The typing effect */
        @keyframes typing-b {
            0% {
                width: 0%;
                opacity: 1;
            }

            100% {
                width: 35%;
                opacity: 1;
            }
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height ">
        @if (Route::has('login'))
        <div class="top-right links">
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>
            <a href="{{ route('register') }}">Register</a>
            @endauth
        </div>
        @endif

        <div class="container p-0 m-0">
                <div class="wrapper">
                    <div class="conversation-wrapper">
                        <div class="row justify-content-end py-2 mx-4">
                            <div class="col-auto to">
                                <p class="mb-0">
                                    Hey man! how you doing?
                                </p>
                                <small>a moment ago...</small>
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="row justify-content-start py-2 mx-4">
                            <div class="col-auto from">
                                <p class="mb-0">
                                    I'm cool bro.
                                </p>
                                <small>a moment ago...</small>
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="row justify-content-end py-2 mx-4">
                            <div class="col-auto to">
                                <p class="mb-0">
                                    Are you going to the baseball game tonight?
                                </p>
                                <small>a moment ago...</small>
                                <div class="triangle"></div>
                            </div>
                        </div>
                        <div class="row justify-content-start py-2 mx-4">
                            <div class="col-auto from">
                                <p class="mb-0">
                                    of course man!!!
                                </p>
                                <small>a moment ago...</small>
                                <div class="triangle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row py-2 px-4">
                    <div class="col-12 input p-3">
                        <span class="message-b">I'm cool bro.</span>
                        <span class="message-d">of course man!!!</span>
                    </div>
                </div>
            </div>
        <div class="content">
            <div class="title m-b-md">
                Chat App
            </div>

            <div class="links">
                <a href="https://github.com/laravel/laravel">GitHub Project</a>
            </div>

        </div>
        
    </div>
    <!-- scripts -->
    <script src="{{ asset('js/animation.js') }}"></script>

</body>

</html>