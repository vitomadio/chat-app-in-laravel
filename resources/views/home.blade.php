@extends('layouts.app')

@section('content')
<div class="h-100" id="app">
	<chat-app :contact="{{ $lastContact }}" :user="{{ auth()->user() }}">
	</chat-app>
</div>
@endsection