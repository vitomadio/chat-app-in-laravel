@extends('layouts.app')

@section('content')
<div class="container h-100">
    <div class="back-link">
        <a href="/">
            <i class="fa fa-chevron-left"></i>
            Home
        </a>
    </div>
    <div class="row justify-content-center align-items-center h-100">
        <div class="col-12 col-md-8 col-lg-6">
            <div class="card p-3 shadow">
                <div class="card-header border-bottom-0 bg-white text-center">
                    <h2>
                        {{ __('Login') }}
                    </h2>
                </div>

                <div class="card-body justify-content-center">
                    <form method="POST" action="{{ route('login') }}"
                        aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-8 offset-2">
                                <label for="email"
                                    class="col-form-label">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email" value="{{ old('email') }}"
                                    required autofocus>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-2">
                                <label for="password"
                                    class="col-form-label">{{ __('Password') }}</label>

                                <input id="password" type="password"
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" required>

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-10 col-md-8 offset-2">
                                <div class="form-check">
                                    <input class="form-check-input"
                                        type="checkbox" name="remember"
                                        id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label"
                                        for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0 justify-content-center">
                            <div class="col-10 col-md-8">
                                <div class="row">
                                    <div class="col-12 col-lg-4">
                                        <button type="submit"
                                            class="btn btn-primary btn-block">
                                            {{ __('Login') }}
                                        </button>
                                    </div>

                                    <div
                                        class="col-12 col-lg-8 text-center text-lg-right">
                                        <a class="btn btn-link text-primary"
                                            href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center mt-2">
                            <div class="col-8 text-center">
                                <a href="/register">Don't have an account
                                    yet?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection